//
//  DetailViewController.swift
//  SongSearch
//
//  Created by Caleb McGuire on 6/13/17.
//  Copyright © 2017 CalebM. All rights reserved.
//

import UIKit
import SafariServices

class DetailViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var artworkImageView: UIImageView!
    @IBOutlet weak var trackLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var albumLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var lyricsWebView: UIWebView!
    @IBOutlet weak var lyricsLabel: UILabel!
    @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var popOutButton: UIButton!


    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            // Make sure outlet is initialized.
            if let label = trackLabel {
                label.text = detail.trackName
                artistLabel.text = detail.artistName
                albumLabel.text = detail.collectionName
                dateLabel.text = detail.releaseDate?.convertToReadableDate()
                artworkImageView.image = detail.artworkThumbnail
                lyricsLabel.text = "Please wait, loading Lyrics Page -"
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        lyricsWebView.delegate = self
        configureView()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        let screenHeight = UIScreen.main.bounds.height
        
        if UIDevice.current.orientation == UIDeviceOrientation.portrait {
            contentViewHeightConstraint.constant = screenHeight + (screenHeight * 0.21)
        } else {
            contentViewHeightConstraint.constant = screenHeight + (screenHeight * 0.46)
        }
    }

    var detailItem: Song? {
        didSet {
            DispatchQueue.global().async {
                if let item = self.detailItem {
                    item.fetchLyricsUrl(artist: item.artistName!, song: item.trackName!) { result in
                        print(result.debugDescription)
                        DispatchQueue.main.async {
                            self.lyricsWebView.loadRequest(result)
                        }
                    }
                }
            }
            // Update the view.
            configureView()
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if !webView.isLoading {
            lyricsLabel.text = "Lyrics Page -"
            let scrollPoint = CGPoint(x: 0, y: (webView.scrollView.contentSize.height * 0.16) - webView.frame.size.height)
            webView.scrollView.setContentOffset(scrollPoint, animated: true)
            popOutButton.isEnabled = true
        }
    }
    
    @IBAction func popOutTap(_ sender: Any) {
        if let lyricUrlString = detailItem?.lyricsUrl {
            if let lyricUrl = URL(string: lyricUrlString) {
                let safariView = SFSafariViewController.init(url: lyricUrl)
                self.present(safariView, animated: true, completion: nil)
            }
        }
    }

}

// MARK: - To convert date.
extension String {
    func convertToReadableDate() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date = dateFormatter.date(from: self)
        
        dateFormatter.dateFormat = "d MMMM yyyy"
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: date!)
        
        return timeStamp
    }
}


