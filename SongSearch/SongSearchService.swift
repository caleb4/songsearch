//
//  SongSearchService.swift
//  SongSearch
//
//  Created by Caleb on 6/13/17.
//  Copyright © 2017 CalebM. All rights reserved.
//

import Foundation
import Alamofire

class SongSearchService {
    static let shared = SongSearchService()
    fileprivate let baseUrl = "https://itunes.apple.com/search?term="
    fileprivate let songEntity = "&entity=song"
    var songResults = [Song]()
    let noResultSong = Song(kind: "song", artistName: "", collectionName: "", trackName: "No Result", previewUrl: "", artworkUrl100: "", releaseDate: "2005-03-01T08:00:00Z", primaryGenreName: "")
    
    func search(for term: String, completion: @escaping ([Song]) -> Void ) {
        
        if let encodedTerm = term.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)?.lowercased() {
            let fullUrl = baseUrl + encodedTerm + songEntity
            
            Alamofire.request(fullUrl).responseJSON { response in
                print(response.data as Any)     // server data
                print(response.result)   // result of response serialization
                self.songResults.removeAll()
                
                guard let result = response.result.value as? [String : Any],
                    let songs = result["results"] as? [AnyObject]
                    else {
                        print("Malformed data received from song api.")
                        self.songResults.append(self.noResultSong)
                        completion(self.songResults)
                        return
                    }
            
                for songResult in songs {
                    if let kind = songResult["kind"] as? String,
                        let artistName = songResult["artistName"] as? String,
                        let collectionName = songResult["collectionName"] as? String,
                        let trackName = songResult["trackName"] as? String,
                        let previewUrl = songResult["previewUrl"] as? String,
                        let artworkUrl100 = songResult["artworkUrl100"] as? String,
                        let releaseDate = songResult["releaseDate"] as? String,
                        let primaryGenreName = songResult["primaryGenreName"] as? String
                    {
                        let song = Song(kind: kind, artistName: artistName, collectionName: collectionName, trackName: trackName, previewUrl: previewUrl, artworkUrl100: artworkUrl100, releaseDate: releaseDate, primaryGenreName: primaryGenreName)
                        self.songResults.append(song)
                    }
                }
                if self.songResults.isEmpty {
                    self.songResults.append(self.noResultSong)
                }
                completion(self.songResults)
            }
        } else {
            print("Term encode failed.")
        }
    }
}
