//
//  Song.swift
//  SongSearch
//
//  Created by Caleb McGuire on 6/13/17.
//  Copyright © 2017 CalebM. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class Song {
    var kind: String?
    var artistName: String?
    var collectionName: String?
    var trackName: String?
    var previewUrl: String?
    var artworkUrl100: String?
    var releaseDate: String?
    var primaryGenreName: String?
    var artworkThumbnail: UIImage?
    var lyricsUrl: String?
    
    init(kind: String, artistName: String, collectionName: String, trackName: String, previewUrl: String, artworkUrl100: String, releaseDate: String, primaryGenreName: String) {
        self.kind = kind
        self.artistName = artistName
        self.collectionName = collectionName
        self.trackName = trackName
        self.previewUrl = previewUrl
        self.artworkUrl100 = artworkUrl100
        self.releaseDate = releaseDate
        self.primaryGenreName = primaryGenreName
    }
    
    func artworkFromUrl(url: String, completion: @escaping () -> Void) {
        Alamofire.request(url).responseData { response in
            if let data = response.result.value,
            let image = UIImage(data: data) {
                self.artworkThumbnail = image
                completion()
            } else {
                self.artworkThumbnail = UIImage()
                completion()
            }
        }
    }
    
}

extension Song {
    // http://lyrics.wikia.com/api.php?func=getSong&artist=Tom+Waits&song=new+coat+of+paint&fmt=json
    // The above endpoint example does not provide valid json data! - I am resorting to trimming out the lyrics url string.
    func fetchLyricsUrl(artist: String, song: String, completion: @escaping (URLRequest) -> Void) {
        let baseUrl = "http://lyrics.wikia.com/api.php?func=getSong&artist="
        let songSpec = "&song="
        let formatSpec = "&fmt=json"
        // tested removing puncuation from strings - removing only ',' seems to produce more success.
        // let trimmedChars = CharacterSet.punctuationCharacters
        // let parsedSong = song.components(separatedBy: trimmedChars).joined(separator: "")
        let parsedSong = song.replacingOccurrences(of: ",", with: "")
        let parsedArtist = artist.replacingOccurrences(of: ",", with: "")
        if let encodedArtist = parsedArtist.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)?.lowercased(),
            let encodedSong = parsedSong.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)?.lowercased() {
            
            let fullUrl = baseUrl + encodedArtist + songSpec + encodedSong + formatSpec
            
            Alamofire.request(fullUrl).responseString { response in
                print(response.data as Any)     // server data
                print(response.result.value.debugDescription)   // result of response serialization
                var urlString = ""
                if let value = response.result.value {
                    if let urlRange = value.range(of: "http://lyrics.wikia.com") {
                        urlString = value.substring(from: urlRange.lowerBound)
                        let portionToTrim = urlString.trimmingCharacters(in: .urlQueryAllowed)
                        urlString = urlString.replacingOccurrences(of: portionToTrim, with: "")
                        urlString.remove(at: urlString.index(before: urlString.endIndex))
                    }
                }
                self.lyricsUrl = urlString
                if let url = URL(string: urlString) {
                    let request = URLRequest(url: url)
                    completion(request)
                }
            }
        }
    }
}
