//
//  MasterViewController.swift
//  SongSearch
//
//  Created by Caleb McGuire on 6/13/17.
//  Copyright © 2017 CalebM. All rights reserved.
//

import UIKit
import Alamofire

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var songResults = [Song]()
    let searchController = UISearchController(searchResultsController: nil)
    let networkManager = NetworkReachabilityManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        setupListener()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let song = songResults[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = song
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songResults.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let song = songResults[indexPath.row]
        cell.textLabel?.text = song.trackName
        cell.detailTextLabel?.text = song.artistName
        if let artworkUrl = song.artworkUrl100 {
            DispatchQueue.global().async {
                song.artworkFromUrl(url: artworkUrl) {
                    DispatchQueue.main.async {
                        cell.imageView?.contentMode = .scaleAspectFit
                        cell.imageView?.image = song.artworkThumbnail
                        cell.setNeedsLayout() //invalidate current layout
                        cell.layoutIfNeeded() //update immediately
                    }
                }
            }
        }
        
        return cell
    }


}

// MARK: - UISearchBarDelegate methods
extension MasterViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        DispatchQueue.global(qos: .userInitiated).async {
            SongSearchService.shared.search(for: searchBar.text!) { result in
                print(result.debugDescription)
                DispatchQueue.main.async {
                    self.tableView.isUserInteractionEnabled = false
                    self.songResults.removeAll()
                    self.songResults = result
                    self.tableView.reloadData()
                    self.tableView.isUserInteractionEnabled = true
                }
            }
        }
    }
}

// MARK: - Reachability
extension MasterViewController {
    func setupListener() {
        networkManager?.listener = { status in
            print("Network Status Changed: \(status)")
            if (self.networkManager?.isReachable)! {
                self.searchController.searchBar.isUserInteractionEnabled = true
                self.searchController.searchBar.placeholder = "Search"
            } else {
                self.searchController.searchBar.placeholder = "A network connection is required."
                self.searchController.searchBar.isUserInteractionEnabled = false
            }
        }
        networkManager?.startListening()
    }
}
