//
//  SongSearchTests.swift
//  SongSearchTests
//
//  Created by Caleb McGuire on 6/13/17.
//  Copyright © 2017 CalebM. All rights reserved.
//

import XCTest
@testable import SongSearch
import Alamofire

class SongSearchTests: XCTestCase {
    
    var masterVC: MasterViewController!
    var detailVC: DetailViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard = UIStoryboard(name: "Main",
                                      bundle: Bundle.main)
        let splitViewController = storyboard.instantiateInitialViewController() as! UISplitViewController
        let navigationVCMaster = splitViewController.viewControllers[0] as! UINavigationController
        let navigationVCDetail = splitViewController.viewControllers[1] as! UINavigationController
        masterVC = navigationVCMaster.topViewController as! MasterViewController
        detailVC = navigationVCDetail.topViewController as! DetailViewController
        
        UIApplication.shared.keyWindow!.rootViewController = splitViewController
        
        // Test and Load the View at the Same Time!
        XCTAssertNotNil(splitViewController.view)
        XCTAssertNotNil(navigationVCMaster.view)
        XCTAssertNotNil(masterVC.view)
        XCTAssertNotNil(navigationVCDetail.view)
        XCTAssertNotNil(detailVC.view)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSearch() {
        XCTAssertNotNil(masterVC.searchController)
        masterVC.searchController.searchBar.text = "Jack Johnson"
        masterVC.searchBarSearchButtonClicked(masterVC.searchController.searchBar)
        let expectation = XCTestExpectation(description: "testTableCellAndDetailView")
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            let path = IndexPath(row: 0, section: 0)
            XCTAssertNotNil(self.masterVC.tableView)
            XCTAssertNotNil(self.masterVC.tableView.cellForRow(at: path))
            self.masterVC.tableView.selectRow(at: path, animated: true, scrollPosition: .none)
            let song = self.masterVC.songResults[path.row]
            self.detailVC.detailItem = song
            self.masterVC.performSegue(withIdentifier: "showDetail", sender: self.masterVC)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 6) {
            XCTAssertNotNil(self.detailVC.view)
            XCTAssert(self.detailVC.trackLabel.text == "Better Together")
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 9) {
            self.detailVC.popOutTap(self.detailVC.popOutButton)
            expectation.fulfill()
        }

        
        wait(for: [expectation], timeout: 10)
    }
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}
